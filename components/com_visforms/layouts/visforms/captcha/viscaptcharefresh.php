<?php
/**
 * @author       Aicha Vack
 * @package      Joomla.Site
 * @subpackage   com_visforms
 * @link         http://www.vi-solutions.de
 * @license      GNU General Public License version 2 or later; see license.txt
 * @copyright    2019 vi-solutions
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

if (!empty($displayData)) :
	if (isset($displayData['form'])) :
		$form = $displayData['form'];
		$class = 'captcharefresh' . $form->id . ((isset($displayData['class'])) ? ' ' . $displayData['class'] : '');
		echo '<img alt="' . JText::_('COM_VISFORMS_REFRESH_CAPTCHA') . '" class="' . $class . '" src="' . JURI::root(true) . '/components/com_visforms/captcha/images/refresh.gif' . '" align="absmiddle" style="cursor:pointer"> &nbsp;';
	endif;
endif;